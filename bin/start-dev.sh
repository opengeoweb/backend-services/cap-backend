#!/bin/bash

uvicorn --host 0.0.0.0 \
    --port=8080 \
    --workers=1 \
    --access-log \
    --reload \
    "src.app:app"
