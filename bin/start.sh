#!/bin/bash

uvicorn --host 0.0.0.0 \
    --port=8080 \
    --workers=4 \
    --access-log \
    "src.app:app"
