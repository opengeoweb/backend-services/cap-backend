import pytest

from tests.test_util import assert_json_equal, read_file


@pytest.mark.no_whitelist
def test_empty_whitelist(monkeypatch, client, test_url):
    monkeypatch.setenv('url_whitelist', '[]')
    response = client.get("/feedlinks?url=" + test_url)
    assert response.status_code == 422
    assert_json_equal(response.text, read_file("res_invalid_cap_url.json"))


@pytest.mark.no_whitelist
def test_whitelist_without_test_url(monkeypatch, client, test_url):
    monkeypatch.setenv('url_whitelist',
                       '["https://foo.com", "https://bar.com"]')
    response = client.get("/feedlinks?url=" + test_url)
    assert response.status_code == 422
    assert_json_equal(response.text, read_file("res_invalid_cap_url.json"))


@pytest.mark.no_whitelist
def test_whitelist_with_at_character(monkeypatch, client, test_url):
    monkeypatch.setenv('url_whitelist', '["https://test.test"]')
    response = client.get("/feedlinks?url=" + test_url + '@www.example.com')
    assert response.status_code == 422
    assert_json_equal(response.text, read_file("res_invalid_cap_url.json"))


def test_whitelist_endpoint(client, test_url):
    response = client.get("/whitelist?url=" + test_url)
    assert response.status_code == 200
    assert_json_equal(response.text, read_file("res_whitelist.json"))
