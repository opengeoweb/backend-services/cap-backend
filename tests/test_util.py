import json

from src.utils import read_resource


def load_json(filename):
    return json.loads(read_file(filename))


def read_file(filename):
    return read_resource("tests.data", filename)


def assert_json_equal(left, right):
    assert json.loads(left) == json.loads(right)


def mock_request(httpx_mock, test_url, api_response, endpoint_response):
    httpx_mock.add_response(
        method="GET",
        url=test_url,
        text=read_file(api_response)
    )
    return read_file(endpoint_response)
