import json

import pytest

from src.geocode_utils import (emma_geocode_json_to_dict,
                               geocode_alias_csv_to_dict, geocode_to_geometry)
from src.utils import open_resource, read_resource


@pytest.fixture(name="emma_geometries")
def geocode_emma_geometries() -> dict[str, str]:
    test_content = read_resource("tests.data", "test_geocodes.json")
    return emma_geocode_json_to_dict(test_content)


@pytest.fixture(name="geocode_aliases")
def geocode_alias_dict() -> dict[str, dict[str, str]]:
    with open_resource("src.geocodes", "geocodes-aliases.csv") as content:
        return geocode_alias_csv_to_dict(content)


def test_lookup_fr001(emma_geometries: dict[str, str]):
    geojson = read_resource("tests.data", "test_emma_id_fr001_geometry.json")
    assert emma_geometries["FR001"] == json.loads(geojson)


def test_lookup_it002(emma_geometries: dict[str, str]):
    geojson = read_resource("tests.data", "test_emma_id_it002_geometry.json")
    assert emma_geometries["IT002"] == json.loads(geojson)


def test_lookup_nuts3(geocode_aliases: dict[str, dict[str, str]]):
    assert geocode_aliases.get("NUTS3").get("BG312") == "BG009"


def test_lookup_warncell(geocode_aliases: dict[str, dict[str, str]]):
    assert geocode_aliases.get("WARNCELL").get("908215999") == "DE109"


def test_lookup_nuts3_geometry():
    geojson = read_resource("tests.data", "test_emma_id_fr015_geometry.json")
    assert geocode_to_geometry("NUTS3", "FR532") == json.loads(geojson)


def test_lookup_nuts2_geometry():
    geojson = read_resource("tests.data", "test_emma_id_gr016_geometry.json")
    assert geocode_to_geometry("NUTS2", "EL43") == json.loads(geojson)
