import json

import pytest
import xmltodict
from fastapi.testclient import TestClient

from src.app import app
from src.create_geojson import (create_coordinates_from_polygons,
                                create_feature, create_generic_alert,
                                create_geojson)
from src.models import (CAPFeatureCollectionModel, CAPFeatureModel,
                        CAPSingleAlertCombinedModel)
from tests.test_util import load_json, read_file


@pytest.fixture
def fastapi_client():
    return TestClient(app)


def test_coordinates_from_polygon_list():
    area = load_json("area_polygon_list.json")
    coordinates = create_coordinates_from_polygons(area)
    assert coordinates == [[[4.4548, 61.0], [4.5137, 60.7568],
                            [4.7958, 60.2412], [4.0, 61.0], [4.4548, 61.0]],
                           [[1.0065, 61.0068], [2.4917, 61.0008],
                            [3.4897, 61.006], [0.0008, 60.9992],
                            [1.0065, 61.0068]]]


def test_coordinates_from_polygon_string():
    area = load_json("area_polygon_string.json")
    coordinates = create_coordinates_from_polygons(area)
    assert coordinates == [[[1.0065, 61.0068], [2.4917, 61.0008],
                            [3.4897, 61.006], [4.0, 61.0], [3.995, 58.9988],
                            [4.0, 58.5], [0.0, 58.5], [0.0008, 60.9992],
                            [1.0065, 61.0068]]]


def test_create_feature():
    area = load_json("area_polygon_string.json")
    info = load_json("info.json")
    expected_feature = CAPFeatureModel(**load_json("res_feature.json"))
    feature = CAPFeatureModel(**create_feature("test", info, area, []))
    assert feature == expected_feature


def test_create_geojson():
    alerts_list = read_file("alerts_list.json")
    expected_geojson = CAPFeatureCollectionModel(
        **load_json("res_geojson.json"))
    geojson = create_geojson(alerts_list)
    assert geojson == expected_geojson


@pytest.mark.noautofixt
def test_multiple_info_alert(fastapi_client):
    with fastapi_client as _:
        alert_txt = read_file("alert_with_multiple_infos.xml")
        alert_dict = xmltodict.parse(alert_txt)
        feature_collection = create_geojson(json.dumps([alert_dict]))
        generic_alert = create_generic_alert(alert_dict)
        cap_model = CAPSingleAlertCombinedModel(
            features=feature_collection.model_dump(by_alias=True),
            alert=generic_alert.model_dump())
        assert len(cap_model.features.features.pop().geometry.geometries) == 4


def test_create_geojson_with_empty_json():
    expected_feature = CAPFeatureCollectionModel(
        **load_json("res_no_warnings.json"))
    feature = create_geojson("{}")
    assert feature == expected_feature
