import httpx

from tests.test_util import assert_json_equal, mock_request, read_file


def test_feed_links_endpoint_with_atom(client, httpx_mock, test_url):
    expected_response = mock_request(httpx_mock, test_url, "feed.xml", "res_feedlinks.json")
    response = client.get("/feedlinks?url=" + test_url)
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)


def test_feed_links_endpoint_with_meteoalarm_atom(client, httpx_mock,
                                                  test_url):
    expected_response = mock_request(httpx_mock, test_url, "feed_meteoalarm.xml", "res_feedlinks_meteoalarm.json")
    response = client.get("/feedlinks?url=" + test_url)
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)


def test_feed_links_endpoint_with_rss(client, httpx_mock, test_url):
    expected_response = mock_request(httpx_mock, test_url, "feed.rss", "res_feedlinks.json")
    response = client.get("/feedlinks?url=" + test_url)
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)


def test_poll_feed_endpoint_with_atom(client, httpx_mock, test_url):
    expected_response = mock_request(httpx_mock, test_url, "feed.xml", "res_poll_feed.json")
    response = client.get("/poll_feed?url=" + test_url)
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)


def test_poll_feed_endpoint_with_meteoalarm_atom(client, httpx_mock, test_url):
    expected_response = mock_request(httpx_mock, test_url, "feed_meteoalarm.xml", "res_poll_feed_meteoalarm.json")
    response = client.get("/poll_feed?url=" + test_url)
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)


def test_poll_feed_endpoint_with_rss(client, httpx_mock, test_url):
    expected_response = mock_request(httpx_mock, test_url, "feed.rss", "res_poll_feed.json")
    response = client.get("/poll_feed?url=" + test_url)
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)


def test_poll_feed_with_invalid_url(client):
    response = client.get("/poll_feed?url=test")
    assert response.status_code == 422


def test_feedlinks_endpoint_with_timeout(client, httpx_mock, test_url):
    httpx_mock.add_exception(
        method="GET",
        url=test_url,
        exception=httpx.TimeoutException("Connection timed out")
    )
    response = client.get("/feedlinks?url=" + test_url)
    assert response.status_code == 500
    assert_json_equal(response.text, read_file("res_error.json"))


def test_feedlinks_endpoint_with_connection_error(client, httpx_mock,
                                                  test_url):
    httpx_mock.add_exception(
        method="GET",
        url=test_url,
        exception=httpx.ConnectError("Connection error")
    )
    response = client.get("/feedlinks?url=" + test_url)
    assert response.status_code == 500
    assert_json_equal(response.text, read_file("res_error.json"))
