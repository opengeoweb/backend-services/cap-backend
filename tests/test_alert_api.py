import json
import logging

import httpx
import pytest
from pyexpat import ExpatError

from src.alert_api import get_all_alerts, get_warnings_from_feed
from src.utils import HTTPXClientWrapper
from tests.test_util import assert_json_equal, mock_request, read_file


def test_single_alert_endpoint(client, httpx_mock, test_url):
    alert_xml = read_file("cap_alert.xml")
    expected_response = read_file("res_cap_single_alert.json")


    httpx_mock.add_response(
        url=test_url,
        method="GET",
        content=alert_xml,
        headers={"Content-Type": "application/xml"},
    )

    response = client.get(f"/singlealert?url={test_url}")
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)



def test_single_alert_endpoint_with_multiple_areas(client, httpx_mock,
                                                   test_url):
    alert_xml = read_file("cap_alert_multi_area.xml")
    expected_response = read_file("res_cap_single_alert_multi_area.json")

    httpx_mock.add_response(
        url=test_url,
        method="GET",
        content=alert_xml,
        headers={"Content-Type": "application/xml"},
    )

    response = client.get("/singlealert?url=" + test_url)
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)


def test_single_alert_endpoint_multi_polygon_area(client, httpx_mock,
                                                  test_url):
    alert_xml = read_file("cap_alert_multi_polygon.xml")
    expected_response = read_file("res_cap_single_alert_multi_polygon.json")

    httpx_mock.add_response(
        url=test_url,
        method="GET",
        content=alert_xml,
        headers={"Content-Type": "application/xml"},
    )

    response = client.get("/singlealert?url=" + test_url)
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)


def test_single_alert_endpoint_emma_id(client, httpx_mock, test_url):
    alert_xml = read_file("cap_alert_meteoalarm_emma_id.xml")
    expected_response = read_file("res_cap_single_alert_emma_id.json")

    httpx_mock.add_response(
        url=test_url,
        method="GET",
        content=alert_xml,
        headers={"Content-Type": "application/xml"},
    )

    response = client.get("/singlealert?url=" + test_url)
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)


def test_single_alert_endpoint_nuts3(client, httpx_mock, test_url):
    alert_xml = read_file("cap_alert_meteoalarm_nuts3.xml")
    expected_response = read_file("res_cap_single_alert_nuts3.json")

    httpx_mock.add_response(
        url=test_url,
        method="GET",
        content=alert_xml,
        headers={"Content-Type": "application/xml"},
    )

    response = client.get("/singlealert?url=" + test_url)
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)


def test_single_alert_endpoint_with_404_response(client, httpx_mock,
                                                 test_url):
    httpx_mock.add_response(
        method="GET",
        url=test_url,
        text="Not Found",
        status_code=404
    )

    response = client.get("/singlealert?url=" + test_url)
    assert response.status_code == 200
    assert_json_equal(response.text,
                      read_file("res_single_alert_no_warnings.json"))


def test_single_alert_endpoint_without_url_param(client):
    response = client.get("/singlealert")
    assert response.status_code == 422


def test_single_alert_endpoint_with_timeout(client, httpx_mock, test_url):
    httpx_mock.add_exception(
        method="GET",
        url=test_url,
        exception=httpx.TimeoutException("Connection timed out")
    )
    response = client.get("/singlealert?url=" + test_url)
    assert response.status_code == 500
    assert_json_equal(response.text, read_file("res_error.json"))


def test_single_alert_endpoint_with_connection_error(client, httpx_mock,
                                                     test_url):
    httpx_mock.add_exception(
        method="GET",
        url=test_url,
        exception=httpx.ConnectError("Connection error")
    )

    response = client.get("/singlealert?url=" + test_url)
    assert response.status_code == 500
    assert_json_equal(response.text, read_file("res_error.json"))


def test_all_alerts_endpoint_with_rss(client, httpx_mock, test_url):

    expected_response = mock_request(httpx_mock, test_url, "all_alerts_feed.rss", "res_allalerts.json")
    alert_1 = read_file("cap_alert_1_for_all_alerts.xml")
    alert_2 = read_file("cap_alert_2_for_all_alerts.xml")
    httpx_mock.add_response(
        method="GET",
        url="https://api.met.no/weatherapi/metalerts/1.1/?cap=2.49.0.1.578.0.221025100000.7008_1_0",
        text=alert_1
    )
    httpx_mock.add_response(
        method="GET",
        url="https://api.met.no/weatherapi/metalerts/1.1/?cap=2.49.0.1.578.0.221025100000.7009_1_0",
        text=alert_2
    )

    response = client.get(f"/allalerts?url={test_url}")
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)

def test_all_alerts_endpoint_with_meteoalarm_atom(client, httpx_mock,
                                                  test_url):
    expected_response = mock_request(httpx_mock, test_url,
                                     "feed_meteoalarm_portugal.xml",
                                     "res_allalerts_meteoalarm.json")
    alert_1 = read_file("cap_alert_meteoalarm_1_for_all_alerts.xml")
    alert_2 = read_file("cap_alert_meteoalarm_2_for_all_alerts.xml")
    alert_3 = read_file("cap_alert_meteoalarm_3_for_all_alerts.xml")
    httpx_mock.add_response(
        method="GET",
        url="https://feeds.meteoalarm.org/api/v1/warnings/feeds-portugal/8c5bbccd-b346-4a71-98cd-234b03a89b80",
        text=alert_1
    )
    httpx_mock.add_response(
        method="GET",
        url="https://feeds.meteoalarm.org/api/v1/warnings/feeds-portugal/0d9eb6e7-2402-4b5d-968b-9a0cbe53b22d",
        text=alert_2
    )
    httpx_mock.add_response(
        method="GET",
        url="https://feeds.meteoalarm.org/api/v1/warnings/feeds-portugal/c2199f30-ba67-4503-880c-c7f9ab438f3c",
        text=alert_3
    )
    response = client.get("/allalerts?url=" + test_url)
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)


def test_all_alerts_endpoint_with_404_responses(client, httpx_mock, test_url, caplog):
    # Mock the initial RSS feed request
    expected_response = mock_request(httpx_mock, test_url, "all_alerts_feed.rss", "res_no_warnings.json")

    httpx_mock.add_response(
        method="GET",
        url="https://api.met.no/weatherapi/metalerts/1.1/?cap=2.49.0.1.578.0.221025100000.7008_1_0",
        text='Not Found',
        status_code=404
    )
    httpx_mock.add_response(
        method="GET",
        url="https://api.met.no/weatherapi/metalerts/1.1/?cap=2.49.0.1.578.0.221025100000.7009_1_0",
        text='Not Found',
        status_code=404
    )

    response = client.get(f"/allalerts?url={test_url}")
    assert response.status_code == 200
    assert_json_equal(response.text, expected_response)
    assert caplog.record_tuples == [
        ("src.alert_api", logging.ERROR,
         ('Failed to fetch alert'
          ' <https://api.met.no/weatherapi/metalerts/1.1/?cap=2.49.0.1.578.0.221025100000.7008_1_0>'
          ' from feed <https://test.test>')),
        ("src.alert_api", logging.ERROR,
         ('Failed to fetch alert'
          ' <https://api.met.no/weatherapi/metalerts/1.1/?cap=2.49.0.1.578.0.221025100000.7009_1_0>'
          ' from feed <https://test.test>'))
    ]

@pytest.mark.asyncio
async def test_get_all_alerts_with_invalid_url():
    with pytest.raises(httpx.UnsupportedProtocol):
        await get_all_alerts("test")

@pytest.mark.asyncio
async def test_feed_with_invalid_content(httpx_mock, test_url):
    httpx_mock.add_response(url=test_url, text="some non-xml content")
    with pytest.raises(ExpatError):
        await get_warnings_from_feed(test_url)
