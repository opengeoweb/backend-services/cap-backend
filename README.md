# Cap Backend

The first experimental version of the CAP backend for development purposes. Can be used to develop CAP components for a GeoWeb (frontend) application.

## Table of contents

[[_TOC_]]

## Running the application

The CAP backend can be run with Docker or as a FastAPI application.

### Build Docker image

Build an image from a Dockerfile.

```
docker build -t cap-backend .
```

### Run with Docker

Create a container from the image and start the container.

```
docker run -p 8080:8080 -d cap-backend
```

## Local installation and development

### Install Python and Dependencies

#### 1. Install Pyenv with Pyenv installer

Check the [prerequisites](https://github.com/pyenv/pyenv/wiki#suggested-build-environment) from the pyenv wiki. for Debian Linux they are:

```bash
sudo apt update; sudo apt install build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
```

Run the Pyenv installer script:

```bash
curl https://pyenv.run | bash
```

Setup your shell environment with these [instructions](https://github.com/pyenv/pyenv#set-up-your-shell-environment-for-pyenv)

#### 2. Build and install Python

With Pyenv installed, check the available Python versions with

```bash
pyenv install -l
```

Install the latest Python 3.11 version from the list and set it as global Python version for your system.

```bash
pyenv install 3.11.8
pyenv global 3.11.8
```

#### 3. Install Pipx

Pipx is a helper utility for installing system wide Python packages, adding them to your path automatically.

Install pipx with these [instructions](https://pipx.pypa.io/latest/installation/). Linux users can install it with pip:

```bash
python3 -m pip install --user pipx
python3 -m pipx ensurepath

# Optional
sudo pipx ensurepath --global
```

#### 4. Install Poetry

Poetry is used for managing project dependencies and metadata. Install Poetry with

```bash
pipx install poetry
```

and add poetry export plugin to it

```bash
pipx inject poetry poetry-plugin-export
```

#### 5. Activate virtualenv and install dependencies

Activate the virtualenv with poetry

```bash
poetry shell
```

And install project dependencies

```bash
poetry install
```

### Using pre-commit

You can also use the pre-commit configuration. This is used to inspect the code that's about to be committed, to see if you've forgotten something, to make sure tests run, or to examine whatever you need to inspect in the code. To use pre-commit make sure the pip install command has succesfully ran with the requirements-dev.txt argument.
Then execute the following command in the terminal in the root folder:

```
pre-commit install
```

This will install the pre-commit functions and now pre-commit will run automatically on git commit.
Only changed files are taken into consideration.
Optionally you can check all the files with the pre-commit checks:

```
pre-commit run --all-files
```

If you wish to skip the pre-commit test but do have pre-commit setup, you can use the --no-verify flag in git to skip the pre-commit checks. For example:

```
git commit -m "Message" --no-verify
```

### Start the Python FastAPI application

`uvicorn --port 8080 src.app:app`

### Managing Python dependencies

With Poetry installed from previous step, you can add or update Python dependencies with poetry add command.

```bash
# adding dependency
poetry add fastapi

# updating dependency
poetry update fastapi
```

You can add development dependencies with --group keyword.

```bash
# adding development dependency
poetry add --group dev isort
```

All poetry operations respect version constraints set in pyproject.toml file.

##### Fully updating dependencies

You can see a list of outdated dependencies by running

```bash
poetry show --outdated
```

To update all dependencies, still respecting the version constraints on pyproject.toml, run

```bash
poetry update
```

### Dependabot integration

This repository has a Dependabot integration with the following behavior specified in the `.gitlab/dependabot.yml` file:

| definition               | behavior                                                                                                                                                                                                                                                                   |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| schedule block           | Scan the repository daily at random time between 6-11. Add any pending package update as MR and update any MR with merge conflicts by doing a rebase. If there is a newer release of the dependency that already has an open MR, make a new MR and close the previous one. |
| open-pull-requests-limit | Don't open more than 3 MRs per directory, even if there are more package updates available than 3.                                                                                                                                                                         |
| rebase-strategy          | Always rebase open MRs, if there are new merged changes on the main branch, even if there are no merge conflicts present.                                                                                                                                                  |

More detailed guide on how to work with dependabot can be found in the [opengeoweb wiki](https://gitlab.com/groups/opengeoweb/-/wikis/How-to-work-with-Dependabot-MRs)


## Endpoints

Endpoints and example requests. See also [OpenAPI schema and API reference](#openapi-schema-and-api-reference).

### Single alert

- Returns a CAP alert in GeoJSON format
  http://localhost:8080/singelalert?url=<alert_url>
- Example requests
  - https://cap.opengeoweb.com/singlealert?url=https://alerts.fmi.fi/cap/2022/05-30/11-35-47Z/2.49.0.1.246.0.0.2022.12915347.135028946423993948468655417678612294058.xml

### All alerts

- Returns all CAP alerts from the feed in GeoJSON format
  http://localhost:8080/allalerts?url=<feed_url>
- Example requests
  - https://cap.opengeoweb.com/allalerts?url=https://alerts.fmi.fi/cap/feed/rss_en-GB.rss
  - https://cap.opengeoweb.com/allalerts?url=https://alerts.fmi.fi/cap/feed/atom_en-GB.xml

### CAP feed polling

- Returns last updated value from the CAP Feed in JSON format
  http://localhost:8080/poll_feed?url=<feed_url>
- Example requests
  - https://cap.opengeoweb.com/poll_feed?url=https://alerts.fmi.fi/cap/feed/rss_en-GB.rss
  - https://cap.opengeoweb.com/poll_feed?url=https://alerts.fmi.fi/cap/feed/atom_en-GB.xml

### Alert links

- Returns alert links from the CAP feed in JSON format
  http://localhost:8080/feedlinks?url=<feed_url>
- Example requests
  - https://cap.opengeoweb.com/feedlinks?url=https://alerts.fmi.fi/cap/feed/rss_en-GB.rss
  - https://cap.opengeoweb.com/feedlinks?url=https://alerts.fmi.fi/cap/feed/atom_en-GB.xml

### Health check

- Returns OK, indicatig that the application is running and is accessible
  http://localhost:8080/healthcheck

## OpenAPI schema and API reference

Generated [OpenAPI](https://github.com/OAI/OpenAPI-Specification) documentation can be browsed at `/docs`.
See http://localhost:8080/docs when running the backend locally or browse the production docs at https://cap.opengeoweb.com/docs.

The OpenAPI schema is available at http://localhost:8080/openapi.json.

Alternative ReDoc API documentation can be viewed at http://localhost:8080/redoc.

## CAP feed and alert URL whitelist

The application has a whitelist for the allowed CAP feed and alert URLs. URLs given as parameters to endpoints have to start with one of the whitelisted URLs. A whitelist URL can contain schema, host and path. The default whitelist can be overridden using an environment variable or a .env file.

Set the whitelist in an environment variable or in a `.env` file:
```
url_whitelist='["https://foo.com", "https://bar.com/path"]'
```

## Coding standards

### Formatting

This project follows the [Google Python Style Guide](https://github.com/google/styleguide/blob/gh-pages/pyguide.md). [YAPF](https://github.com/google/yapf) is used used to enforce this. Install all dependencies using `pip install -r requirements.txt -r requirements-dev.txt` and run YAPF by executing
```
yapf -ir src/
```
This will format all Python files in [src](src), using the configuration specified in [.style.yapf](.style.yapf).

### Organizing imports

Imports are organized using isort. Run
```
isort src/
```

### Installing tools globally
Many python tools, including yapf and isort, can be installed globally using pipx, or package managers such as apt. Run

```bash
pipx install yapf isort
```

or if you don't have pipx,

```bash
sudo apt install yapf isort
```
The tools can be run with the same commands as above.

### IDE support

If you are using Visual Studio Code, consider updating [.vscode/settings.json](.vscode/settings.json) with the contents below. This will format code and organize imports on save.

```
{
    "editor.codeActionsOnSave": {
        "source.organizeImports": true
    },
    "python.formatting.provider": "yapf",
    "[python]": {
        "editor.formatOnSave": true
    }
}
```

### Running tests

Tests are implemented with [pytest](https://pytest.org). Run the tests by executing `pytest`. If you are using Visual Studio Code, tests can be run from the testing view.

## Deployment

When MR gets merged, the GitLab CI/CD pipeline builds a container image based on the [Dockerfile](./Dockerfile)

- Docker image is published to the GitLab Container Registry (https://gitlab.com/opengeoweb/backend-services/cap-backend/container_registry)
- The docker image is tagged with a tag `"registry.gitlab.com/opengeoweb/backend-services/cap-backend:main"`
- Note: the main branch based version of the Cap backend will be deployed to the development environment later when it is available. See https://gitlab.com/opengeoweb/opengeoweb/-/issues/2417

When a new tag is created from the main branch (use semantic versioning for example v0.0.1)

- Docker image is published to GitLab Container Registry (https://gitlab.com/opengeoweb/backend-services/cap-backend/container_registry)
- The docker image is tagged with a tag `"registry.gitlab.com/opengeoweb/backend-services/cap-backend:v0.0.1"`
- A new revision of the Elastic Container Service (ECS) task definition is created and uploaded to the ECS
- The new tagged version of the Cap backend gets deployed to the ECS production environment (https://cap.opengeoweb.com/)
