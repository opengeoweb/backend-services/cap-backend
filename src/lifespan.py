# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)

from contextlib import asynccontextmanager

from fastapi import FastAPI

from src.geocode_utils import (emma_geocode_json_to_dict,
                               geocode_alias_csv_to_dict)
from src.geocodes.store import geocodes
from src.utils import HTTPXClientWrapper, open_resource, read_resource


@asynccontextmanager
async def lifespan(_app: FastAPI):
    await HTTPXClientWrapper.get_httpx_client()
    geocodes["emma"] = emma_geocode_json_to_dict(
        read_resource("src.geocodes", "geocodes.json"))
    with open_resource("src.geocodes", "geocodes-aliases.csv") as content:
        geocodes["aliases"] = geocode_alias_csv_to_dict(content)
    yield
    geocodes.clear()
    await HTTPXClientWrapper.close_httpx_client()
