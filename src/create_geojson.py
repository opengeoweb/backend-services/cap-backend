import json
import logging
from typing import Dict, List, Optional

from deepdiff import DeepDiff
from geojson_rewind import rewind

from src.geocode_utils import geocode_to_geometry
from src.models import AlertModel, CAPFeatureCollectionModel

logger = logging.getLogger(__name__)


def create_coordinates_from_polygons(area: Dict):
    coordinate_lists = []

    if isinstance(area['polygon'], list):
        for polygon in area['polygon']:
            coordinate_list = []
            coordinates = polygon.split(" ")
            # Flip coordinates x->y, y->x
            for coordinate_pair in coordinates:
                single_coordinate = coordinate_pair.split(",")
                if len(single_coordinate) > 1:
                    flipped_coordinate = [
                        float(single_coordinate[1]),
                        float(single_coordinate[0])
                    ]
                    coordinate_list.append(flipped_coordinate)
            coordinate_lists.append(coordinate_list)

    # If polygon coordinates are just a string
    else:
        coordinate_list = []
        split_coordinates = area['polygon'].split(" ")
        for coordinate_pair in split_coordinates:
            single_coordinate = coordinate_pair.split(",")
            if len(single_coordinate) > 1:
                flipped_coordinate = [
                    float(single_coordinate[1]),
                    float(single_coordinate[0])
                ]
                coordinate_list.append(flipped_coordinate)
        coordinate_lists.append(coordinate_list)

    return coordinate_lists


def create_feature(identifier: str, info: Dict, area: Dict,
                   languages: List) -> Optional[Dict]:
    geometry = None
    if 'polygon' in area:
        coordinate_lists = create_coordinates_from_polygons(area)
        geometry_type = None
        coordinates = []

        if len(coordinate_lists) > 1:
            geometry_type = 'MultiPolygon'
            for coordinate_list in coordinate_lists:
                coordinates.append([coordinate_list])
        else:
            geometry_type = 'Polygon'
            coordinates = coordinate_lists

        geometry = {'type': geometry_type, 'coordinates': coordinates}
    elif 'geocode' in area:
        if isinstance(area.get("geocode"), list):
            geometry = {"type": "GeometryCollection", "geometries": []}
            for geocode in area.get("geocode"):
                geometry["geometries"].append(
                    geocode_to_geometry(geocode.get("valueName"),
                                        geocode.get("value")))

        else:
            geocode = area.get("geocode")
            geometry = geocode_to_geometry(geocode.get("valueName"),
                                           geocode.get("value"))

    if not geometry:
        logger.error('Could not produce geometry for area <%s>', area)
        return None

    return rewind({
        'type': 'Feature',
        'properties': {
            'fill': '',
            'stroke': '',
            'stroke-width': 2,
            'stroke-opacity': 1,
            'details': {
                'identifier': identifier,
                'severity': info.get('severity', ''),
                'certainty': info.get('certainty', ''),
                'onset': info.get('onset', ''),
                'expires': info.get('expires', ''),
                'languages': languages
            },
        },
        'geometry': geometry
    })


def create_geojson(alerts_list_json: str) -> CAPFeatureCollectionModel:
    alert_list = json.loads(alerts_list_json)
    features = []

    for alert in alert_list:
        identifier = alert["alert"].get('identifier', '')
        infos = get_info_as_list(alert)

        for info in infos:
            areas = info["area"]
            if not isinstance(areas, list):
                areas = [areas]
            for index, area in enumerate(areas):
                languages = create_languages(alert, index)
                # pylint: disable=using-assignment-expression-in-unsupported-version
                if feature := create_feature(identifier, info, area, languages):
                    add_feature_no_duplicates(feature, features)

    geojson = {'type': 'FeatureCollection', 'features': features}
    return CAPFeatureCollectionModel(**geojson)


def create_generic_alert(alert: Dict) -> AlertModel:
    info = get_first_info(alert)
    languages = create_languages(alert)
    transformed_json = {
        'identifier': alert["alert"].get('identifier', ''),
        'onset': info.get('onset', ''),
        'expires': info.get('expires', ''),
        'severity': info.get('severity', ''),
        'certainty': info.get('certainty', ''),
        'languages': languages
    }
    return AlertModel(**transformed_json)


def create_languages(alert: Dict, area_index: int = None):
    languages = []
    info = alert["alert"]["info"]
    if isinstance(info, list):
        for alert_info in info:
            languages.append(create_language(alert_info, area_index))
    else:
        languages.append(create_language(info, area_index))
    return languages


def create_language(info: Dict, area_index: int = None):
    area = info["area"]
    if isinstance(area, list):
        if isinstance(area_index, int):
            area_desc = area[area_index].get("areaDesc", "")
        else:
            area_desc = ', '.join([a.get("areaDesc", "") for a in area])
    else:
        area_desc = area.get("areaDesc", "")
    return {
        'language': info.get('language', ''),
        'event': info.get('event', ''),
        'senderName': info.get('senderName', ''),
        'headline': info.get('headline', ''),
        'description': info.get('description', ''),
        'areaDesc': area_desc,
        "parameter": info.get("parameter", [])
    }


def get_info_as_list(alert: Dict) -> List:
    info = alert["alert"]["info"]
    return info if isinstance(info, list) else [info]


def get_first_info(alert: Dict):
    info = alert["alert"]["info"]
    return info[0] if isinstance(info, list) else info


def add_feature_no_duplicates(feature, feature_list):
    for list_member in feature_list:
        diff = DeepDiff(list_member, feature)
        if len(diff) == 0:
            return

    feature_list.append(feature)
