# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2022 - Finnish Meteorological Institute (FMI)

import json
import logging

import xmltodict
from fastapi import APIRouter, Query

from src.create_geojson import create_generic_alert, create_geojson
from src.models import (CAPFeatureCollectionModel, CAPSingleAlertCombinedModel,
                        EmptyModel, ErrorModel)
from src.utils import (HTTPXClientWrapper, get_alert_links,
                       get_warnings_from_feed)
from src.whitelist import CAPUrl

alertRouter = APIRouter()
logger = logging.getLogger(__name__)


async def get_combined_alert(alert_url: CAPUrl):
    client = await HTTPXClientWrapper.get_httpx_client()
    response = await client.get(url=alert_url)
    if (response.status_code == 200) and response.text:
        alert_dict = xmltodict.parse(response.text)
        if alert_dict.get("alert", {}).get("identifier"):
            feature_collection = create_geojson(json.dumps([alert_dict]))
            generic_alert = create_generic_alert(alert_dict)
            return CAPSingleAlertCombinedModel(
                features=feature_collection.model_dump(by_alias=True),
                alert=generic_alert.model_dump())

    logger.warning('No alerts found from url <%s>', alert_url)
    empty_feature_collection = {'type': 'FeatureCollection', 'features': []}
    empty_alert = EmptyModel()
    return CAPSingleAlertCombinedModel(features=empty_feature_collection,
                                       alert=empty_alert.model_dump())


async def get_all_alerts(feed_url: CAPUrl):
    client = await HTTPXClientWrapper.get_httpx_client()
    warnings_list = await get_warnings_from_feed(feed_url)
    alert_links = get_alert_links(warnings_list)

    alerts_list = []
    for link in alert_links:
        alert_response = await client.get(link)
        if (alert_response.status_code == 200) and (alert_response.text):
            alert_dict = xmltodict.parse(alert_response.text)
            if alert_dict.get("alert", {}).get("identifier"):
                alerts_list.append(alert_dict)
        else:
            logger.error('Failed to fetch alert <%s> from feed <%s>', link,
                         feed_url)

    # _____ Converts all alerts into geoJson and send response back  _____
    return create_geojson(json.dumps(alerts_list))


@alertRouter.get("/singlealert",
                 response_model=CAPSingleAlertCombinedModel,
                 response_model_exclude_none=True,
                 responses={
                     500: {
                         "model": ErrorModel,
                     },
                 })
async def combined_alert(url: CAPUrl = Query(description="CAP alert URL")):
    "Returns CAP alert in generic format and map-compatible GeoJSON format"
    return await get_combined_alert(url)


@alertRouter.get("/allalerts",
                 response_model=CAPFeatureCollectionModel,
                 response_model_exclude_none=True,
                 responses={
                     500: {
                         "model": ErrorModel,
                     },
                 })
async def all_alerts(url: CAPUrl = Query(description="CAP feed URL")):
    "Returns all CAP alerts in GeoJSON"
    return await get_all_alerts(url)
