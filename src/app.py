# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2022 - Finnish Meteorological Institute (FMI)

import logging
import os
import sys
from typing import Callable

import uvicorn
from fastapi import APIRouter, FastAPI, Request, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse, PlainTextResponse

from src.alert_api import alertRouter
from src.feed_api import feedRouter
from src.healthcheck_endpoint import healthRouter
from src.lifespan import lifespan
from src.models import ErrorModel
from src.settings import settings
from src.whitelist import whitelistRouter

logging.basicConfig(
    stream=sys.stdout,
    level=logging.WARNING,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
logger = logging.getLogger(__name__)

app = FastAPI(lifespan=lifespan,
              root_path=settings.application_root_path,
              docs_url=settings.application_doc_root,
              title="GeoWeb CAP Backend",
              version=os.getenv('APP_VERSION', 'dev'),
              description="GeoWeb CAP Backend API")


@app.middleware("http")
async def add_headers(request: Request, call_next):
    """Adds security headers"""
    response = await call_next(request)
    response.headers['X-Content-Type-Options'] = "nosniff"
    response.headers[
        'Strict-Transport-Security'] = "max-age=31536000; includeSubDomains"
    return response


@app.middleware("http")
async def fallback_exception_handler(request: Request, fn: Callable):
    try:
        return await fn(request)
    except Exception:  # pylint: disable=broad-exception-caught
        logger.exception("message")
        return JSONResponse(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            content=ErrorModel(message='Something went wrong').model_dump())


origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=["*"],
    allow_headers=["*"],
)

main = APIRouter()


@main.get("/", response_class=PlainTextResponse)
def get_root():
    "Returns a welcome message"
    return PlainTextResponse('GeoWeb CAP Backend')


app.include_router(main)
app.include_router(healthRouter)
app.include_router(whitelistRouter)
app.include_router(alertRouter)
app.include_router(feedRouter)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)
