from typing import Annotated, Optional, Union

from annotated_types import Len, MinLen
from geojson_pydantic import (Feature, FeatureCollection, GeometryCollection,
                              MultiPolygon, Polygon)
from pydantic import BaseModel, ConfigDict, Field

# Lists are used for coordinates because Tuples are not supported in the OpenAPI
# version that FastAPI uses.
# See https://github.com/developmentseed/geojson-pydantic/issues/42 for details.
#
# Some of the custom pydantic Field usage is just to remove extra fields from the
# generated OpenAPI spec because they are not allowed by the version that FastAPI uses.
# See https://github.com/tiangolo/fastapi/issues/3745 for details.

NumType = Union[float, int]

CAPBBox = Annotated[list[NumType], Len(4, 4)]

CAPPosition = Annotated[list[NumType], Len(2, 2)]

CAPLinearRing = Annotated[list[CAPPosition], MinLen(4)]

CAPPolygonCoords = Annotated[list[CAPLinearRing], MinLen(1)]

CAPMultiPolygonCoords = Annotated[list[CAPPolygonCoords], MinLen(1)]


class LanguageModel(BaseModel):
    language: str
    event: str
    senderName: str
    headline: str
    description: str
    areaDesc: str
    parameter: list[dict]


class DetailsModel(BaseModel):
    identifier: str
    severity: str
    certainty: str
    onset: str
    expires: str
    languages: list[LanguageModel]


class PropertiesModel(BaseModel):
    fill: str
    stroke: str
    stroke_width: float = Field(alias='stroke-width')
    stroke_opacity: float = Field(alias='stroke-opacity')
    details: DetailsModel


class CAPPolygon(Polygon):
    type: str = Field("Polygon")
    coordinates: CAPPolygonCoords


class CAPMultiPolygon(MultiPolygon):
    type: str = Field("MultiPolygon")
    coordinates: CAPMultiPolygonCoords


class CAPFeatureModel(Feature):
    type: str = Field("Feature")
    geometry: Optional[Union[CAPPolygon, CAPMultiPolygon, GeometryCollection]]
    properties: Optional[PropertiesModel]
    id: Optional[str] = None
    bbox: Optional[CAPBBox] = None


class CAPFeatureCollectionModel(FeatureCollection):
    type: str = Field("FeatureCollection")
    features: list[CAPFeatureModel]
    bbox: Optional[CAPBBox] = None


class LastUpdatedModel(BaseModel):
    last_updated: int = Field(alias='lastUpdated')


class ErrorModel(BaseModel):
    message: str


class EmptyModel(BaseModel):
    model_config = ConfigDict(extra="forbid")


class AlertModel(BaseModel):
    identifier: str
    onset: str
    expires: str
    severity: str
    certainty: str
    languages: list[LanguageModel]


class CAPSingleAlertCombinedModel(BaseModel):
    features: CAPFeatureCollectionModel
    alert: Union[AlertModel, EmptyModel]
