from functools import lru_cache
from typing import Set

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_file='.env',
                                      env_file_encoding='utf-8')

    url_whitelist: Set[str] = {
        "https://api.met.no/weatherapi/metalerts/",
        "https://alerts.fmi.fi/cap/", "https://feeds.meteoalarm.org/",
        "https://api01.nve.no/hydrology/forecast/"
    }

    application_root_path: str = ""
    application_doc_root: str = "/api"


settings = Settings()


@lru_cache()
def get_settings() -> Settings:
    return Settings()


@lru_cache()
def get_whitelist_urls() -> tuple[str]:
    return tuple(get_settings().url_whitelist)
