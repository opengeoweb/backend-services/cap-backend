from typing import Annotated, Set

from fastapi import APIRouter, Depends, HTTPException, Query
from pydantic import BeforeValidator, HttpUrl, TypeAdapter

from src.settings import Settings, get_settings, get_whitelist_urls

whitelistRouter = APIRouter()

http_url_adapter = TypeAdapter(HttpUrl)
CAPUrl = Annotated[
    str,
    BeforeValidator(lambda value: str(http_url_adapter.validate_python(value)))]


def validate_cap_url(url: str = Query(...)):
    """Validate CAP URL by ensuring that it is in the whitelist and 
    does not contain an '@' character

    Args:
        url (str, optional): The provided CAP Url. Defaults to Query(...).

    Raises:
        HTTPException: If the URL is not in the whitelist or contains an '@' character

    Returns:
        str: The validated URL
    """
    if not str(url).startswith(get_whitelist_urls()) or '@' in url:
        raise HTTPException(status_code=422,
                            detail={
                                "loc": ["query", "url"],
                                "msg": "invalid CAP URL",
                                "type": "value_error.invalidcapurl"
                            })
    return url


@whitelistRouter.get("/whitelist", response_model=Set[str])
def whitelist(settings: Settings = Depends(get_settings)):
    "Returns the whitelist for CAP feed and alert base URLs"
    return settings.url_whitelist
