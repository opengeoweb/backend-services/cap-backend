import csv
import json
import logging
from typing import Iterable, Union

from src.geocodes.store import geocodes

logger = logging.getLogger(__name__)


def emma_geocodes() -> dict[str, dict]:
    return geocodes["emma"]


def geocode_aliases() -> dict[str, dict[str, str]]:
    return geocodes["aliases"]


def geocode_to_geometry(geocode_type: str, code: str) -> Union[dict, None]:
    if geocode_type == "EMMA_ID":
        return emma_geocodes().get(code)
    return emma_geocodes().get(geocode_aliases().get(geocode_type,
                                                     {}).get(code))


def emma_geocode_json_to_dict(geocode_json: str) -> dict[str, str]:
    content = json.loads(geocode_json)
    lookup_dict = {}
    for feature in content["features"]:
        properties = feature["properties"]
        if properties["type"] != "EMMA_ID":
            logger.warning('Geocode type is not EMMA_ID in <%s>:', feature)
            continue
        emma_id: str = properties["code"]
        geometry: str = feature["geometry"]
        lookup_dict[emma_id] = geometry
    return lookup_dict


def geocode_alias_csv_to_dict(
        geocode_csv: Iterable[str]) -> dict[str, dict[str, str]]:
    lookup_dict = {}
    reader = csv.DictReader(geocode_csv, delimiter=',')
    for row in reader:
        geocode_type: str = row["ALIAS_TYPE"]
        if not geocode_type in lookup_dict:
            lookup_dict[geocode_type] = {}
        lookup_dict[geocode_type][row["ALIAS_CODE"]] = row["CODE"]
    return lookup_dict
