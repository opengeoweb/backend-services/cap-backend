# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright 2022 - Finnish Meteorological Institute (FMI)

import datetime
import logging

import xmltodict
from dateutil import parser
from fastapi import APIRouter, Depends

from src.models import ErrorModel, LastUpdatedModel
from src.utils import (HTTPXClientWrapper, get_alert_links,
                       get_warnings_from_feed)
from src.whitelist import CAPUrl, validate_cap_url

feedRouter = APIRouter()
logger = logging.getLogger(__name__)


async def get_last_updated(feed_url: CAPUrl):
    client = await HTTPXClientWrapper.get_httpx_client()
    response = await client.get(feed_url)
    feed_dict = {}
    if (response.status_code == 200) and (response.text):
        feed_dict = xmltodict.parse(response.text)
    last_updated = {"lastUpdated": ""}
    if feed_dict.get("rss", {}).get("channel", {}).get("lastBuildDate"):
        # lastBuildDate (optional) is the last time the content of the channel changed
        # in RFC 822 format
        last_build_date = parser.parse(
            feed_dict.get("rss", {}).get("channel", {}).get("lastBuildDate"))
        last_updated["lastUpdated"] = int(round(last_build_date.timestamp()))
    elif feed_dict.get("rss", {}).get("channel", {}).get("pubDate"):
        # pubDate (optional) is the publication date for the content in the channel
        # in RFC 822 format
        pub_date = parser.parse(
            feed_dict.get("rss", {}).get("channel", {}).get("pubDate"))
        last_updated["lastUpdated"] = int(round(pub_date.timestamp()))
    elif feed_dict.get("feed", {}).get("updated"):
        # updated (required) indicates the last time the feed was modified
        # in a significant way in RFC 3339 format
        # (which is a subset of ISO 8601)
        updated = parser.parse(feed_dict.get("feed", {}).get("updated"))
        last_updated["lastUpdated"] = int(round(updated.timestamp()))
    else:
        # return current time as a backup
        utc_now = datetime.datetime.now(datetime.timezone.utc)
        timestamp = int(round(utc_now.timestamp()))
        last_updated["lastUpdated"] = timestamp
    return last_updated


@feedRouter.get("/feedlinks",
                response_model=list[str],
                responses={
                    500: {
                        "model": ErrorModel,
                    },
                },
                description="CAP feed URL to fetch feed links from")
async def cap_feed_links(url: CAPUrl = Depends(validate_cap_url)):
    "Returns alert links from the CAP Feed in JSON"
    return get_alert_links(await get_warnings_from_feed(url))


@feedRouter.get(
    "/poll_feed",
    response_model=LastUpdatedModel,
    responses={
        200: {
            "description":
                "JSON with the last updated time as a unix timestamp",
        },
        500: {
            "model": ErrorModel,
        },
    },
    description="CAP feed URL to poll for the last updated time")
async def poll_feed(url: CAPUrl = Depends(validate_cap_url)):
    "Returns last updated value from the CAP Feed in JSON"
    return await get_last_updated(url)
