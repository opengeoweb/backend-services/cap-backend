from __future__ import annotations

import logging
from typing import Optional, TextIO, Union

import httpx
import importlib_resources
import xmltodict

from src.whitelist import CAPUrl

logger = logging.getLogger(__name__)


class HTTPXClientWrapper(httpx.AsyncClient):
    _USER_AGENT = {'User-Agent': 'GeoWeb/0.1'}
    _DEFAULT_TIMEOUT = 60  # seconds
    _instance: HTTPXClientWrapper | None = None  # singleton

    def __init__(self, timeout: float = _DEFAULT_TIMEOUT):
        timeout = httpx.Timeout(timeout)
        super().__init__(headers=self._USER_AGENT, timeout=timeout)

    @classmethod
    async def get_httpx_client(cls, timeout: float = _DEFAULT_TIMEOUT):
        """
        Returns the singleton HTTPXClientWrapper
        """
        if cls._instance is None:
            cls._instance = cls(timeout=timeout)
        return cls._instance

    @classmethod
    async def close_httpx_client(cls):
        """
        Closes the shared HTTPX client instance.
        """
        if cls._instance is not None:
            await cls._instance.aclose()
            cls._instance = None


async def get_warnings_from_feed(feed_url: CAPUrl) -> list[dict]:
    client = await HTTPXClientWrapper.get_httpx_client()
    response = await client.get(feed_url)
    warnings = {}
    if (response.status_code == 200) and response.text:
        feed_dict = xmltodict.parse(response.text)
        if feed_dict.get("rss", {}).get("channel", {}).get("item"):
            warnings = feed_dict.get("rss", {}).get("channel", {}).get("item")
        elif feed_dict.get("feed", {}).get("entry"):
            warnings = feed_dict.get("feed", {}).get("entry")

    if not warnings:
        logger.warning('No  warnings found for feed <%s>:', feed_url)
    if isinstance(warnings, list):
        return warnings
    return [warnings]


def get_alert_links(warnings: list[dict]) -> list[str]:
    alert_links = []
    if warnings:
        for alert in warnings:
            # pylint: disable = using-assignment-expression-in-unsupported-version
            if links := alert.get("link"):
                if alert_link := select_alert_link(links):
                    alert_links.append(alert_link)

    # Remove duplicates which some feeds provide. Order is maintained.
    return list(dict.fromkeys(alert_links))


def select_alert_link(links: Union[str, dict[str], list[str]]) -> Optional[str]:
    if isinstance(links, str):
        return links
    if isinstance(links, dict):
        return links.get("@href", None)
    if isinstance(links, list):
        alert_link = next((link for link in links
                           if link.get("@type") == "application/cap+xml"), None)
        return select_alert_link(alert_link)
    return None


def read_resource(path: str, filename: str):
    return importlib_resources.files(path).joinpath(filename).read_text("utf-8")


def open_resource(path: str, filename: str) -> TextIO:
    return importlib_resources.files(path).joinpath(filename).open(
        encoding="utf-8")
