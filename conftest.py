# This file provides test autodiscovery and global test fixtures

import pytest
from fastapi.testclient import TestClient

import src.geocode_utils
from src.app import app
from src.settings import get_settings, get_whitelist_urls
from src.utils import open_resource, read_resource


@pytest.fixture(autouse=True)
def url_whitelist_setup(monkeypatch, request):
    if 'no_whitelist' in request.keywords:
        # Clear the LRU cache so that the marked test functions
        # can set their own whitelists and avoid the cached version
        get_settings.cache_clear()
        get_whitelist_urls.cache_clear()
        return
    monkeypatch.setenv('url_whitelist', '["https://test.test"]')


@pytest.fixture(autouse=True)
def set_test_emma_geocodes(request, monkeypatch):
    if 'noautofixt' in request.keywords:
        return
    with open_resource("src.geocodes", "geocodes-aliases.csv") as aliases:
        geojson = read_resource("tests.data", "test_geocodes.json")
        geocodes = {
            'emma': src.geocode_utils.emma_geocode_json_to_dict(geojson),
            'aliases': src.geocode_utils.geocode_alias_csv_to_dict(aliases)
        }
        monkeypatch.setattr(src.geocode_utils, "geocodes", geocodes)


@pytest.fixture()
def client():
    return TestClient(app)


@pytest.fixture()
def test_url():
    return "https://test.test"
