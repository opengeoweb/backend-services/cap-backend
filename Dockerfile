FROM python:3.11-slim-bookworm AS base

ARG VERSION=dev
ENV APP_VERSION=$VERSION
ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache \
    POETRY_VIRTUALENVS_CREATE=1

RUN mkdir -p /app/src

COPY src/ app/src

ADD poetry.lock /app
ADD pyproject.toml /app
ADD README.md /app

RUN apt-get update && apt-get install -y curl bash && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*

WORKDIR /app

EXPOSE 8080

RUN pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir poetry

RUN poetry install --without dev --no-root && rm -rf $POETRY_CACHE_DIR

ENV PATH="/app/.venv/bin:$PATH"

HEALTHCHECK CMD curl --fail http://localhost:8080/healthcheck || exit 1

RUN useradd --create-home worker
USER worker

CMD ["uvicorn", "--host", "0.0.0.0", "--port", "8080", "--workers", "4", "--access-log", "src.app:app"]
